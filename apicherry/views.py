from django.http import JsonResponse
import requests


class TwitchAPI(object):
    client_id = '3lq37qni1m2ev1amfkqitrt1yqux8o'  # cherryentertainment

    def __init__(self):
        pass

    def get_headers(self):
        return {'Client-ID': self.client_id}

    def request(self):
        headers = self.get_headers()
        payload = {
            'language': 'ko',
        }
        r = requests.get('https://api.twitch.tv/helix/streams', headers=headers, params=payload)
        data = r.json()
        return data['data']


def public(request):
    data = TwitchAPI().request()
    viewer_count = 0
    for d in data:
        viewer_count += d["viewer_count"]
    data = {"streamers": data, "viewer_count": viewer_count}
    return JsonResponse(data)

